
import numpy as np
import subprocess as sbp

#### file splitter, div entre test/val/train
def file_splitter_train_test_val(fullsetfile,spltDct={"val":20.0, "test":10.0}):
    ### permite dividir en test/train o test/val/train
    nontrain=0.0
    for key,val in spltDct.items():
        nontrain+=val
    #spltDct["train"]=100.0-nontrain
    
    #fullset=pd.read_csv(fullsetfile)
    fullset=np.loadtxt(fullsetfile ,delimiter=",")
    ## shuffle
    rng=np.random.default_rng()
    rng.shuffle(fullset,axis=0)
    nline,ncols=np.shape(fullset)
    
    #fullset0=[]#np.where(fullset[:,-1]==0, fullset  )
    #fullset1=[]#=np.where(fullset[:,-1]==0, fullset  )
    print("Balance en origen: ",np.sum(fullset[:,-1]))
    print("nline,ncols",nline,ncols)
    fset=set(fullset[:,-1])
    Clasificados=[]
    for val in fset:
        tempset=[]
        for ifi,fila in enumerate(fullset):
           if fila[-1]==val:
               tempset.append(fila[:-1])
               
        Clasificados.append(tempset)
        
    fset=list(fset)
    print(fset)
    
    for ifs,f in enumerate(fset):
        mydir="ECG_"+str(f)+"/"
        cmdstr=" mkdir "+mydir
        sbp.call(cmdstr,shell=True) 
        for ivec,vec in enumerate(Clasificados[ifs]):
            np.savetxt(mydir+"ecg_"+str(f) +"_"+str(ivec).zfill(3) +".dat",vec)
    #ifull=np.arange(nline)
    #random.shuffle(ifull)
    #import sys
    #sys.exit()
    #FileDct={}
    nsub=0
    n0=0
    for key,val in spltDct.items():
        nsub+=int(val/100.0*nline)
        print("n0,nsub",n0,nsub)
        subset=fullset[n0:nsub,:]
        n0=nsub
        print("Balance subset: ",np.sum(subset[:,-1])/np.shape(subset)[0] )
        np.savetxt(key+"_"+fullsetfile,subset,delimiter=",",fmt="%f")
        #FileDct[key]=key+fullsetfile
        #fwrite=open(file=key+fullsetfile,"w")
    subset=fullset[nsub:,:]
    print("remanente:", np.shape(subset))
    print("Balance rem: ",np.sum(subset[:,-1])/np.shape(subset)[0] )
    np.savetxt("train"+"_"+fullsetfile,subset,delimiter=",")
    

file_splitter_train_test_val("ecg.csv")
 
